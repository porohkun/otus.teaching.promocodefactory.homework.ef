﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerPreference> CustomerPreference { get; set; }
        public virtual DbSet<Preference> Preference { get; set; }
        public virtual DbSet<PromoCode> PromoCode { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<T> GetDbSet<T>() where T : BaseEntity
        {
            return GetType().GetProperty(typeof(T).Name).GetValue(this) as DbSet<T>;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var adminRole = new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            };
            var partnerManagerRole = new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            };
            modelBuilder.Entity<Role>().HasData(adminRole, partnerManagerRole);



            var employee1 = new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = adminRole.Id,
                AppliedPromocodesCount = 5
            };
            var employee2 = new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = partnerManagerRole.Id,
                AppliedPromocodesCount = 10
            };
            modelBuilder.Entity<Employee>().HasData(employee1, employee2);



            var theaterPreference = new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            };
            var familyPreference = new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            };
            var childrenPreference = new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            };
            modelBuilder.Entity<Preference>().HasData(theaterPreference, familyPreference, childrenPreference);



            var customer1 = new Customer()
            {
                Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Иван",
                LastName = "Петров",
            };
            modelBuilder.Entity<Customer>().HasData(customer1);



            modelBuilder.Entity<PromoCode>().HasData(new PromoCode()
            {
                Id = Guid.Parse("5E7D1F13-9A68-4266-A9EC-D4303E254B32"),
                Code = "Code1",
                ServiceInfo = "Some info 1",
                BeginDate = new DateTime(2023, 01, 01),
                EndDate = new DateTime(2024, 01, 01),
                PartnerName = "partner1",
                PartnerManagerId = employee1.Id,
                PreferenceId = theaterPreference.Id,
                CustomerId = customer1.Id
            });



            modelBuilder.Entity<CustomerPreference>().HasData(
                new CustomerPreference()
                {
                    Id = Guid.Parse("28FC095D-9F4E-4192-B843-CFA6038A7886"),
                    CustomerId = customer1.Id,
                    PreferenceId = theaterPreference.Id
                },
                new CustomerPreference()
                {
                    Id = Guid.Parse("955C3D8E-51D1-4970-9CFE-F56474DF0650"),
                    CustomerId = customer1.Id,
                    PreferenceId = childrenPreference.Id
                });



            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasOne(e => e.Role)
                      .WithMany()
                      .HasForeignKey("RoleId")
                      .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.HasOne(p => p.Customer)
                      .WithMany(c => c.PromoCodes)
                      .HasForeignKey("CustomerId")
                      .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(p => p.Preference)
                      .WithMany()
                      .HasForeignKey("PreferenceId")
                      .OnDelete(DeleteBehavior.SetNull);

                entity.HasOne(p => p.PartnerManager)
                      .WithMany()
                      .HasForeignKey("PartnerManagerId")
                      .IsRequired(false)
                      .OnDelete(DeleteBehavior.SetNull);
            });

            modelBuilder.Entity<CustomerPreference>(entity =>
            {
                entity.HasOne(p => p.Customer)
                      .WithMany(c => c.CustomerPreferences)
                      .HasForeignKey("CustomerId")
                      .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(p => p.Preference)
                      .WithMany()
                      .HasForeignKey("PreferenceId")
                      .OnDelete(DeleteBehavior.Cascade);

                entity.HasIndex(p => new { p.CustomerId, p.PreferenceId })
                      .IsUnique();
            });

        }
    }
}