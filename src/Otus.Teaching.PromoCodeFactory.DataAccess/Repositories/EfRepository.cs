﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync(Func<IQueryable<T>, IQueryable<T>> expression = null)
        {
            IQueryable<T> query = _dataContext.GetDbSet<T>();

            if (expression != null)
                query = expression(query);

            return await Task.Run(() => query.ToArray());
        }

        public async Task<T> GetByIdAsync(Guid id, Func<IQueryable<T>, IQueryable<T>> expression = null)
        {
            IQueryable<T> query = _dataContext.GetDbSet<T>();

            if (expression != null)
                query = expression(query);

            return await Task.Run(() => query.SingleOrDefault(x => x.Id == id));
        }

        public async Task AddAsync(T entity, bool commit = true)
        {
            _dataContext.GetDbSet<T>().Add(entity);

            if (commit)
                await _dataContext.SaveChangesAsync();
        }


        public async Task UpdateAsync(T entity, bool commit = true)
        {
            _dataContext.GetDbSet<T>().Update(entity);

            if (commit)
                await _dataContext.SaveChangesAsync();
        }

        public async Task RemoveByIdAsync(Guid id, bool commit = true)
        {
            var entity = Activator.CreateInstance<T>();
            entity.Id = id;
            _dataContext.GetDbSet<T>().Remove(entity);

            if (commit)
                await _dataContext.SaveChangesAsync();
        }

        public async Task<int> RemoveAsync(Func<IQueryable<T>, IQueryable<T>> expression = null, bool commit = true)
        {
            IQueryable<T> query = _dataContext.GetDbSet<T>();

            if (expression != null)
                query = expression(query);

            return await Task.Run(() => query.ExecuteDeleteAsync());
        }

        public async Task CommitAsync()
        {
            await _dataContext.SaveChangesAsync();
        }
    }
}