﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync(Func<IQueryable<T>, IQueryable<T>> expression = null)
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id, Func<IQueryable<T>, IQueryable<T>> expression = null)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T entity, bool commit = true)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T entity, bool commit = true)
        {
            throw new NotImplementedException();
        }

        public Task RemoveByIdAsync(Guid id, bool commit = true)
        {
            throw new NotImplementedException();
        }

        public Task<int> RemoveAsync(Func<IQueryable<T>, IQueryable<T>> expression = null, bool commit = true)
        {
            throw new NotImplementedException();
        }

        public Task CommitAsync()
        {
            throw new NotImplementedException();
        }
    }
}