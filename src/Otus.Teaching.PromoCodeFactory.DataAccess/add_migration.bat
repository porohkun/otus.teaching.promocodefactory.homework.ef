@echo off
set /p migrationName=name your migration:
dotnet ef migrations add %migrationName% --startup-project ../Otus.Teaching.PromoCodeFactory.WebHost