﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddedCascadeDeletionStrategyForeSomeFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_PromoCode_Customer_CustomerId",
                table: "PromoCode");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPreference_CustomerId",
                table: "CustomerPreference");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreference_CustomerId_PreferenceId",
                table: "CustomerPreference",
                columns: new[] { "CustomerId", "PreferenceId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId",
                table: "CustomerPreference",
                column: "PreferenceId",
                principalTable: "Preference",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCode_Customer_CustomerId",
                table: "PromoCode",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId",
                table: "CustomerPreference");

            migrationBuilder.DropForeignKey(
                name: "FK_PromoCode_Customer_CustomerId",
                table: "PromoCode");

            migrationBuilder.DropIndex(
                name: "IX_CustomerPreference_CustomerId_PreferenceId",
                table: "CustomerPreference");

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPreference_CustomerId",
                table: "CustomerPreference",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Customer_CustomerId",
                table: "CustomerPreference",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomerPreference_Preference_PreferenceId",
                table: "CustomerPreference",
                column: "PreferenceId",
                principalTable: "Preference",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_PromoCode_Customer_CustomerId",
                table: "PromoCode",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
