﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        [StringLength(128)] public string FirstName { get; set; }
        [StringLength(128)] public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [StringLength(128)] public string Email { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
    }
}