﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role : BaseEntity
    {
        [StringLength(128)] public string Name { get; set; }

        [StringLength(128)] public string Description { get; set; }
    }
}