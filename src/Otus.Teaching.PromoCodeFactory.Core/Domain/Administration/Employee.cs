﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : BaseEntity
    {
        [StringLength(128)] public string FirstName { get; set; }
        [StringLength(128)] public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [StringLength(128)] public string Email { get; set; }

        public Guid RoleId { get; set; }
        public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}