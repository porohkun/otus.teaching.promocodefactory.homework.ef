﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public abstract class BaseEntity
    {
        [Key, Required]
        public Guid Id { get; set; }
    }
}