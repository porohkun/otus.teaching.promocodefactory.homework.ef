﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(Func<IQueryable<T>, IQueryable<T>> expression = null);

        Task<T> GetByIdAsync(Guid id, Func<IQueryable<T>, IQueryable<T>> expression = null);

        Task AddAsync(T entity, bool commit = true);

        Task UpdateAsync(T entity, bool commit = true);

        Task RemoveByIdAsync(Guid id, bool commit = true);

        Task<int> RemoveAsync(Func<IQueryable<T>, IQueryable<T>> expression = null, bool commit = true);

        Task CommitAsync();
    }
}