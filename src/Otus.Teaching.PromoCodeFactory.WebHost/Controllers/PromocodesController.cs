﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Employee> employeeRepository,
            IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _employeeRepository = employeeRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var promoCodesModelList = promoCodes.Select(p =>
                new PromoCodeShortResponse()
                {
                    Id = p.Id,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    BeginDate = p.BeginDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    EndDate = p.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    PartnerName = p.PartnerName,
                });

            return promoCodesModelList;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost("{EmployeeId}")]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(Guid EmployeeId, GivePromoCodeRequest request)
        {
            var customers = (await _customerPreferenceRepository.GetAllAsync(e => e.Where(cp => cp.PreferenceId == request.PreferenceId))).Select(cp => cp.CustomerId).ToArray();
            var employee = await _employeeRepository.GetByIdAsync(EmployeeId);
            if (employee == null) return Unauthorized();

            foreach (var customerId in customers)
            {
                var newPromoCode = new PromoCode()
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PreferenceId = request.PreferenceId,
                    PartnerName = request.PartnerName,
                    BeginDate = DateTime.UtcNow.Date,
                    EndDate = DateTime.UtcNow.Date.AddDays(7),
                    CustomerId = customerId,
                    PartnerManagerId = employee.Id
                };
                await _promoCodeRepository.AddAsync(newPromoCode, false);
            }

            await _promoCodeRepository.CommitAsync();

            return Ok();
        }
    }
}