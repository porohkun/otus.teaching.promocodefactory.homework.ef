﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                });

            return customersModelList;
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerByIdAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id,
                e => e.Include(c => c.PromoCodes)
                      .Include(c => c.CustomerPreferences)
                          .ThenInclude(cp => cp.Preference));

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.CustomerPreferences.Select(p => new PreferenceShortResponse()
                {
                    Id = p.Preference.Id,
                    Name = p.Preference.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes.Select(p => new PromoCodeShortResponse()
                {
                    Id = p.Id,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    BeginDate = p.BeginDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    EndDate = p.EndDate.ToString("yyyy-MM-ddTHH:mm:ss"),
                    PartnerName = p.PartnerName,
                }).ToList(),
            };

            return customerModel;
        }

        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var newCustomer = new Customer()
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            await _customerRepository.AddAsync(newCustomer, false);

            foreach (var preference in request.PreferenceIds.Distinct())
                await _customerPreferenceRepository.AddAsync(new CustomerPreference()
                {
                    CustomerId = newCustomer.Id,
                    PreferenceId = preference
                }, false);
            await _customerRepository.CommitAsync();

            return Ok();
        }

        /// <summary>
        /// Редактировать данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            await _customerRepository.UpdateAsync(customer, false);

            var oldPrefs = (await _customerPreferenceRepository.GetAllAsync(e => e.Where(cp => cp.CustomerId == customer.Id))).Select(cp => cp.PreferenceId).ToArray();
            var newPrefs = request.PreferenceIds;

            foreach (var prefId in oldPrefs.Except(newPrefs))
                await _customerPreferenceRepository.RemoveAsync(e => e.Where(cp => cp.CustomerId == customer.Id && cp.PreferenceId == prefId));

            foreach (var prefId in newPrefs.Distinct().Except(oldPrefs))
                await _customerPreferenceRepository.AddAsync(new CustomerPreference()
                {
                    CustomerId = customer.Id,
                    PreferenceId = prefId
                }, false);

            await _customerRepository.CommitAsync();

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.RemoveByIdAsync(id);
            return Ok();
        }
    }
}